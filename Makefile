.PHONY: docker-compose-build
docker-compose-build:
	docker-compose build
	@$(DONE)

.PHONY: up
up: docker-compose-build
	docker-compose up -d
	@$(DONE)

.PHONY: down
down:
	docker-compose down
	@$(DONE)

.PHONY: docker-shell
docker-shell: docker-compose-build
	docker-compose run --rm wagtail /bin/bash
	@$(DONE)
