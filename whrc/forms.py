from django import forms
from django.forms import ModelForm
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Invisible
from .models import Signature


class SignatureForm(ModelForm):
    captcha = ReCaptchaField(widget=ReCaptchaV2Invisible)

    class Meta:
        model = Signature
        fields = [
            'name',
            'email',
            'subscribe_to_emails',
            'organization',
            'sign_for_organization',
            'country'
        ]
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Jane Doe'}),
            'email': forms.EmailInput(attrs={'placeholder': 'jdoe@example.com'}),
            'organization': forms.TextInput(attrs={'placeholder': 'BigOrg inc.'}),
        }
