from wagtail.core.fields import StreamField
from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock


class SignDeclarationBlock(blocks.StaticBlock):

    def get_context(self, value, parent_context=None):
        from .forms import SignatureForm
        context = super().get_context(value, parent_context)
        context['form'] = SignatureForm()
        return context

    class Meta:
        admin_text = 'Sign the Declaration form: no config'
        template = 'blocks/sign_dec_form.html'


class DeclarationStatsBlock(blocks.StaticBlock):

    def get_context(self, value, parent_context=None):
        from .models import Signature, Organization
        context = super().get_context(value, parent_context)
        context['signature_count'] = Signature.objects.count()
        context['organization_count'] = Organization.objects.count()
        context['country_count'] = Signature.objects.values('country').distinct().count()
        return context

    class Meta:
        admin_text = 'Stats about the Declaration: no config'
        template = 'blocks/declaration_stats.html'


class BodyBlock(blocks.StreamBlock):
    page_divider = blocks.StaticBlock(
        admin_text='Female symbol page divider: no config',
        template='blocks/female_divider.html',
        icon='horizontalrule')
    declaration_stats = DeclarationStatsBlock(icon='list-ol')
    sign_the_declaration = SignDeclarationBlock(icon='edit')
    basic_paragraph = blocks.StructBlock(
        [
            ('title', blocks.CharBlock()),
            ('body', blocks.RichTextBlock()),
        ],
        help_text='A simple paragraph with a title and rich text',
        template='blocks/basic_paragraph.html',
        icon='pilcrow'
    )
    paragraph_with_image = blocks.StructBlock(
        [
            ('title', blocks.CharBlock(required=False)),
            ('body', blocks.RichTextBlock()),
            ('image', ImageChooserBlock()),
        ],
        help_text='A paragraph with a title, rich text, and image',
        template='blocks/paragraph_with_image.html',
        icon='picture'
    )
    cta_block = blocks.StructBlock(
        [
            ('button_text', blocks.CharBlock()),
            ('button_link', blocks.URLBlock())
        ],
        help_text='A cta with external link.',
        template='blocks/cta_std.html',
    )
    country_contact_form = blocks.StaticBlock(
        admin_text='Contry Contact Form: no config',
        template='blocks/country_contact_form.html',
        icon='form')
    volunteer_form = blocks.StaticBlock(
        admin_text='Volunteer Form: no config',
        template='blocks/volunteer_form.html',
        icon='form')
    donate_widget = blocks.StaticBlock(
        admin_text='Donate Widget: no config',
        template='blocks/donate_widget.html',
        icon='pick')
    org_list = blocks.StaticBlock(
        admin_text='Supporting Orgs List: no config',
        template='blocks/org_list.html',
        icon='group')


class HeaderCtaBlock(blocks.StreamBlock):
    cta = blocks.StructBlock([
        ('button_text', blocks.CharBlock()),
        ('button_link', blocks.PageChooserBlock()),
    ],
    icon='redirect'
    )
