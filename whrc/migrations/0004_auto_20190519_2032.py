# Generated by Django 2.2.1 on 2019-05-19 20:32

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.blocks.static_block
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('whrc', '0003_auto_20190519_1941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='standardpage',
            name='body',
            field=wagtail.core.fields.StreamField([('page_divider', wagtail.core.blocks.static_block.StaticBlock(admin_text='Female symbol page divider: no config', template='blocks/female_divider.html')), ('declaration__stats', wagtail.core.blocks.static_block.StaticBlock(admin_text='Stats about the Declaration: no config', template='blocks/declaration_stats.html')), ('sign_the_declaration', wagtail.core.blocks.static_block.StaticBlock(admin_text='Sign the Declaration form: no config', template='blocks/sign_dec_form.html')), ('basic_paragraph', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('body', wagtail.core.blocks.RichTextBlock())], help_text='A simple paragraph with a title and rich text'))], blank=True),
        ),
    ]
