from django.db import migrations, models


def remove_duplicate_emails(apps, schema_editor):
    Signature = apps.get_model("whrc", "Signature")

    all_emails = Signature.objects.values_list('email', flat=True).distinct()

    for email in all_emails:
        signatures = Signature.objects.filter(email=email)
        dupe_ids = signatures.values_list('id', flat=True)[1:]
        dupe_signatures = Signature.objects.filter(pk__in=dupe_ids)
        dupe_signatures.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('whrc', '0022_countrylistpage'),
    ]

    operations = [
        migrations.RunPython(remove_duplicate_emails),
        migrations.AlterField(
            model_name='signature',
            name='email',
            field=models.EmailField(help_text='Your email', max_length=1000, unique=True),
        ),
    ]
