# Generated by Django 2.2.1 on 2019-05-26 18:07

from django.db import migrations, models
import django.db.models.deletion
import wagtail.core.blocks
import wagtail.core.blocks.static_block
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0041_group_collection_permissions_verbose_name_plural'),
        ('whrc', '0006_auto_20190526_1713'),
    ]

    operations = [
        migrations.CreateModel(
            name='SignatureListPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('subtitle', models.CharField(blank=True, help_text='A subtitle to show on the header of the page', max_length=255)),
                ('header_ctas', wagtail.core.fields.StreamField([('cta', wagtail.core.blocks.StructBlock([('button_text', wagtail.core.blocks.CharBlock()), ('button_link', wagtail.core.blocks.PageChooserBlock())]))])),
            ],
            options={
                'abstract': False,
            },
            bases=('wagtailcore.page',),
        ),
        migrations.AlterField(
            model_name='standardpage',
            name='body',
            field=wagtail.core.fields.StreamField([('page_divider', wagtail.core.blocks.static_block.StaticBlock(admin_text='Female symbol page divider: no config', template='blocks/female_divider.html')), ('declaration__stats', wagtail.core.blocks.static_block.StaticBlock(admin_text='Stats about the Declaration: no config', template='blocks/declaration_stats.html')), ('sign_the_declaration', wagtail.core.blocks.static_block.StaticBlock(admin_text='Sign the Declaration form: no config', template='blocks/sign_dec_form.html')), ('basic_paragraph', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock()), ('body', wagtail.core.blocks.RichTextBlock())], help_text='A simple paragraph with a title and rich text', template='blocks/basic_paragraph.html')), ('paragraph_with_image', wagtail.core.blocks.StructBlock([('title', wagtail.core.blocks.CharBlock(required=False)), ('body', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock())], help_text='A paragraph with a title, rich text, and image', template='blocks/paragraph_with_image.html'))]),
        ),
        migrations.AlterField(
            model_name='standardpage',
            name='header_ctas',
            field=wagtail.core.fields.StreamField([('cta', wagtail.core.blocks.StructBlock([('button_text', wagtail.core.blocks.CharBlock()), ('button_link', wagtail.core.blocks.PageChooserBlock())]))]),
        ),
    ]
