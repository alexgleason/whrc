# Generated by Django 2.2.1 on 2019-06-01 23:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('whrc', '0009_auto_20190601_1826'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='homepage',
            field=models.URLField(blank=True, help_text="Link to the organization's homepage"),
        ),
    ]
