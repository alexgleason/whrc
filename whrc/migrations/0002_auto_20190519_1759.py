# Generated by Django 2.2.1 on 2019-05-19 17:59

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.blocks.static_block
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('whrc', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='standardpage',
            name='body',
            field=wagtail.core.fields.StreamField([('page_divider', wagtail.core.blocks.static_block.StaticBlock(admin_text='Female symbol page devider: no configuration needed', template='blocks/female_divider.html')), ('sign_the_declaration', wagtail.core.blocks.static_block.StaticBlock(admin_text='Sign the Declaration form: no configuration needed', template='blocks/sign_dec_form.html')), ('basic_paragraph', wagtail.core.blocks.StreamBlock([('title', wagtail.core.blocks.CharBlock()), ('body', wagtail.core.blocks.RichTextBlock())], help_text='A simple paragraph with a title and rich text', template='blocks/basic_paragraph.html'))]),
        ),
        migrations.AlterField(
            model_name='standardpage',
            name='header_ctas',
            field=wagtail.core.fields.StreamField([('button', wagtail.core.blocks.StructBlock([('button_text', wagtail.core.blocks.CharBlock(classname='button text')), ('button_link', wagtail.core.blocks.PageChooserBlock())]))]),
        ),
    ]
