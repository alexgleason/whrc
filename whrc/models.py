from django.db import models
from django_countries.fields import CountryField
from django.core.paginator import Paginator

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.core import blocks

from wagtail.core.rich_text import RichText
from wagtail.images.blocks import ImageChooserBlock

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel

from wagtailtrans.models import TranslatablePage

from .blocks import BodyBlock, HeaderCtaBlock

from datetime import datetime


class Signature(models.Model):
    name = models.CharField(max_length=255, help_text='Your first and last name')
    email = models.EmailField(max_length=1000, unique=True, help_text='Your email')
    subscribe_to_emails = models.BooleanField(help_text='Get email updates?', blank=True)
    organization = models.CharField(max_length=255, help_text='Your organization, if they are okay with being mentioned', blank=True)
    sign_for_organization = models.BooleanField(help_text='Sign on behalf of this organization?',blank=True)
    country = CountryField(help_text='What is your country?')
    timestamp = models.DateTimeField(auto_now_add=True)


class Organization(models.Model):
    name = models.CharField(max_length=255, help_text='Your first and last name')
    country = CountryField(help_text='Where is this org based?', blank=True)
    international = models.BooleanField(help_text='Is this org international? (will override country)', blank=True)
    logo = models.ImageField(help_text='A square or circle logo')
    homepage = models.URLField(max_length=200, help_text='Link to the organization\'s homepage', blank=True)


class NewsItem(models.Model):
    headline = models.CharField(max_length=255, help_text='The headline of the article')
    preview_text = models.TextField(max_length=1000, help_text='A few lines of preview text for the article')
    image = models.ImageField(help_text='A square image to represent the post')
    article_link = models.URLField(max_length=200, help_text='Link to the original source', blank=True)
    cta_link = models.URLField(max_length=200, help_text='Call to action button link', blank=True, null=True)
    cta_title = models.CharField(max_length=255, help_text='Prompt the user to take action', blank=True, null=True)
    cta_subtitle = models.CharField(max_length=255, help_text='Prompt the user to take action', blank=True, null=True)
    cta_image = models.ImageField(help_text='A square or round logo or image representing the call to action', blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)


class Event(models.Model):
    title = models.CharField(max_length=255, help_text='The title of the event')
    description = models.TextField(help_text='The description of the event')
    location = models.TextField(help_text='The location of the event')
    start_time = models.DateTimeField(help_text='The start time of the event')
    end_time = models.DateTimeField(help_text='The start time of the event')
    image = models.ImageField(help_text='An image that represents the event')
    event_link = models.URLField(max_length=200, help_text='Link to the event signup page', blank=True)


class StandardPage(TranslatablePage):
    subtitle = models.CharField(max_length=255, help_text='A subtitle to show on the header of the page', blank=True)

    header_ctas = StreamField(HeaderCtaBlock(required=False), blank=True)
    body = StreamField(BodyBlock(required=False), blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('subtitle'),
        StreamFieldPanel('header_ctas'),
        StreamFieldPanel('body'),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        signatures = Signature.objects.all().order_by('-timestamp')
        organizations = Organization.objects.all().order_by('name')

        context['signatures'] = signatures
        context['organizations'] = organizations
        return context


class CountryListPage(Page):
    subtitle = models.CharField(max_length=255, help_text='A subtitle to show on the header of the page', blank=True)

    header_ctas = StreamField(HeaderCtaBlock(required=False), blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('subtitle'),
        StreamFieldPanel('header_ctas'),
    ]

    #TODO: cache
    def get_context(self, request):
        context = super().get_context(request)

        signatures = Signature.objects.all().order_by('-timestamp')

        countries = []

        for signature in signatures:
            countries.append(signature.country)

        def getName(item):
            return(item.name)

        context['countries'] = sorted(set(countries),key=getName)
        context['country_count'] = Signature.objects.values('country').distinct().count()

        return context


class SignatureListPage(TranslatablePage):
    subtitle = models.CharField(max_length=255, help_text='A subtitle to show on the header of the page', blank=True)

    header_ctas = StreamField(HeaderCtaBlock(required=False), blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('subtitle'),
        StreamFieldPanel('header_ctas'),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        signatures_list = Signature.objects.all().order_by('-timestamp')
        organizations_list = Organization.objects.all().order_by('name')
        paginator = Paginator(signatures_list, 200)

        page = request.GET.get('page')
        signatures = paginator.get_page(page)

        context['signatures'] = signatures
        context['organizations'] = organizations_list
        return context


class NewsItemListPage(TranslatablePage):
    subtitle = models.CharField(max_length=255, help_text='A subtitle to show on the header of the page', blank=True)

    header_ctas = StreamField(HeaderCtaBlock(required=False), blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('subtitle'),
        StreamFieldPanel('header_ctas'),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        new_items_list = NewsItem.objects.all().order_by('-timestamp')
        paginator = Paginator(new_items_list, 9)

        page = request.GET.get('page')
        news_items = paginator.get_page(page)

        context['news_items'] = news_items
        return context


class EventListPage(TranslatablePage):
    subtitle = models.CharField(max_length=255, help_text='A subtitle to show on the header of the page', blank=True)

    header_ctas = StreamField(HeaderCtaBlock(required=False), blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('subtitle'),
        StreamFieldPanel('header_ctas'),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        events_list = Event.objects.all().order_by('-timestamp')
        now = datetime.now()

        upcoming_events = set()
        past_events = set()

        for e in Event.objects.filter(end_time__gt=now):
            # Without select_related(), this would make a database query for each
            # loop iteration in order to fetch the related blog for each entry.
            # https://docs.djangoproject.com/en/2.2/ref/models/querysets/#select-related
            upcoming_events.add(e)

        for e in Event.objects.filter(end_time__lt=now):
            past_events.add(e)


        context['upcoming_events'] = upcoming_events
        context['past_events'] = past_events
        return context
