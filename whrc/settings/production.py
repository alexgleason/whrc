import os
from .base import *

DEBUG = False

env = os.environ.copy()

if 'SECRET_KEY' in env:
    SECRET_KEY = env['SECRET_KEY']

if 'ALLOWED_HOSTS' in env:
    ALLOWED_HOSTS = env['ALLOWED_HOSTS'].split(',')

# Configure email
# https://docs.djangoproject.com/en/2.0/topics/email/
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_USE_TLS = True
# EMAIL_HOST = env['EMAIL_HOST']
# EMAIL_PORT = int(env.get('EMAIL_PORT', 587))
# EMAIL_HOST_USER = env['EMAIL_HOST_USER']
# EMAIL_HOST_PASSWORD = env['EMAIL_HOST_PASSWORD']
# DEFAULT_FROM_EMAIL = env['DEFAULT_FROM_EMAIL']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'ERROR'),
        },
    },
}

# Makes WhiteNoise work properly.
# http://whitenoise.evans.io/en/stable/django.html#django-compressor
COMPRESS_OFFLINE = True

# Google Analytics Configs
GA_KEY_FILEPATH = env['GA_KEY_FILEPATH']
GA_VIEW_ID = env['GA_VIEW_ID']

# reCAPTCHA
# https://github.com/praekelt/django-recaptcha
RECAPTCHA_PUBLIC_KEY = env['RECAPTCHA_PUBLIC_KEY']
RECAPTCHA_PRIVATE_KEY = env['RECAPTCHA_PRIVATE_KEY']

try:
    from .local import *
except ImportError:
    pass
