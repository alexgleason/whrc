import csv

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages
from django.http.response import HttpResponse

from ratelimit.decorators import ratelimit

from .forms import SignatureForm
from .models import Signature, Organization


@ratelimit(key='ip', rate='100/h')
def submit_signature(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SignatureForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            messages.success(request, 'Thank you for signing the Declaration!')
            # redirect to a new URL:
            return HttpResponseRedirect('/')
        else:
            messages.error(request, 'There was an error processing your form. Please try again.')
            return HttpResponseRedirect('/')
    else:
        return HttpResponse(status_code=400)


def signatures_export_csv(request):
    """
    Export signatures in CSV format.
    """
    # Only admins are allowed
    # FIXME: Use Wagtail's advanced permission system
    if not request.user.is_superuser:
        return HttpResponse(status_code=403)

    # Make the response a downloadable CSV
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="signatures.csv"'

    # Write the header row
    writer = csv.writer(response)
    writer.writerow([
        "Name",
        "Email",
        "Subscribe to emails",
        "Organization",
        "Sign for organization",
        "Country",
        "Timestamp"
    ])

    # Create a row for each signature
    for signature in Signature.objects.all():
        writer.writerow([
            signature.name,
            signature.email,
            signature.subscribe_to_emails,
            signature.organization,
            signature.sign_for_organization,
            signature.country,
            signature.timestamp
        ])

    return response



def organizations_export_csv(request):
    """
    Export organizations in CSV format.
    """
    # Only admins are allowed
    # FIXME: Use Wagtail's advanced permission system
    if not request.user.is_superuser:
        return HttpResponse(status_code=403)

    # Make the response a downloadable CSV
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="organizations.csv"'

    # Write the header row
    writer = csv.writer(response)
    writer.writerow([
        "Name",
        "Country",
        "International",
        "Homepage",
    ])

    # Create a row for each organization
    for organization in Organization.objects.all():
        writer.writerow([
            organization.name,
            organization.country,
            organization.international,
            organization.homepage,
        ])

    return response
