from django.urls import path
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from wagtail.core import hooks

from .models import Signature, Organization, NewsItem, Event
from . import views as whrc_views


class SignatureAdmin(ModelAdmin):
    model = Signature
    menu_label = 'Signatures'  # ditch this to use verbose_name_plural from model
    menu_icon = 'user'  # change as required
    menu_order = 200  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('name', 'country', 'organization', 'timestamp')
    list_filter = ('country', 'organization', 'sign_for_organization', 'subscribe_to_emails')
    search_fields = ('name', 'organization')


class OrganizationAdmin(ModelAdmin):
    model = Organization
    menu_label = 'Organizations'  # ditch this to use verbose_name_plural from model
    menu_icon = 'group'  # change as required
    menu_order = 300  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('name', 'country', 'international')
    list_filter = ('country', 'international')
    search_fields = ('name',)


class NewsItemAdmin(ModelAdmin):
    model = NewsItem
    menu_label = 'News Item'  # ditch this to use verbose_name_plural from model
    menu_icon = 'title'  # change as required
    menu_order = 300  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('headline', 'timestamp')
    search_fields = ('headline', 'preview_text', 'cta_title' 'cta_subtitle')


class EventAdmin(ModelAdmin):
    model = Event
    menu_label = 'Event'  # ditch this to use verbose_name_plural from model
    menu_icon = 'date'  # change as required
    menu_order = 400  # will put in 3rd place (000 being 1st, 100 2nd)
    add_to_settings_menu = False  # or True to add your model to the Settings sub-menu
    exclude_from_explorer = False # or True to exclude pages of this type from Wagtail's explorer view
    list_display = ('title', 'start_time', 'end_time', 'location', 'event_link')
    search_fields = ('title', 'description', 'location' 'event_link')


# Now you just need to register your customised ModelAdmin class with Wagtail
modeladmin_register(SignatureAdmin)
modeladmin_register(OrganizationAdmin)
modeladmin_register(NewsItemAdmin)
modeladmin_register(EventAdmin)


# Signature CSV export URL
@hooks.register('register_admin_urls')
def urlconf_signatures_export_csv():
  return [path(
    'whrc/signature/export/',
    whrc_views.signatures_export_csv,
    name='signatures_export_csv'
  )]


 # Organization CSV export URL
@hooks.register('register_admin_urls')
def urlconf_organizations_export_csv():
  return [path(
     'whrc/organization/export/',
     whrc_views.organizations_export_csv,
     name='organizations_export_csv'
    )]
