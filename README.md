# WHRC Website

> :warning: **Please note:** not a lot of time could be spent on this documentation.
Proceed at your own risk, and be aware that you may have to research stuff just to get the site running.

## Local development

### On Mac:
1. Download and install [Docker Desktop](https://www.docker.com/products/docker-desktop)
   (you will have to create an account)
2. Open the Docker Desktop application on your computer
3. `make up` - this builds and starts a docker container with all needed
   requirements installed, then starts the site running.
4. You can see the locally running version of the site in your browser, at `127.0.0.1:8000`.

### On Linux:
1. Install docker and docker-compose
2. `make up` - this builds and starts a docker container with all needed
   requirements installed, then starts the site running.
3. You can see the locally running version of the site in your browser, at `127.0.0.1:8000`.


You can also visit `/admin` to get into Wagtail's CMS interface. You can login
to admin as a superuser with account name `whrc` and password `whrc_dev_password`

### Shell onto the running server
 - `docker ps` to see the container_id of your whrc_server image
 - `docker exec -it <<container_id>> /bin/bash` to get a shell

### Applying Migrations
If you have made changes to your models that requiring migration, you will have to restart the Docker container:
 - `make down`
 - `make up`
